use curl::easy::Easy;
use std::env;
use std::fs;

extern crate serde;
extern crate serde_json;
extern crate toml;

#[macro_use]
extern crate serde_derive;

#[derive(Deserialize)]
struct Config {
  options: Options,
}

#[derive(Deserialize)]
struct Options {
  url: String,
  city: String,
  key: String,
}

#[derive(Serialize, Deserialize)]
struct WeatherData {
  coord: Coord,
  weather: Vec<Weather>,
  base: String,
  main: Main,
  visibility: i32,
  clouds: Clouds,
  dt: i32,
  id: i32,
  name: String,
  cod: i32,
}

#[derive(Serialize, Deserialize)]
struct Coord {
  lon: f32,
  lat: f32,
}

#[derive(Serialize, Deserialize)]
struct Weather {
  id: i32,
  main: String,
  description: String,
  icon: String,
}

#[derive(Serialize, Deserialize)]
struct Main {
  temp: f32,
  pressure: i32,
  humidity: i32,
  temp_min: f32,
  temp_max: f32,
}

#[derive(Serialize, Deserialize)]
struct Wind {
  speed: f32,
}

#[derive(Serialize, Deserialize)]
struct Clouds {
  all: i32,
}

#[derive(Serialize, Deserialize)]
struct Sys {
  itype: i32,
  id: i32,
  message: f32,
  country: String,
  sunrise: i32,
  sunset: i32,
}

fn kelvin_to_fahrenheit(k: f32) -> f32 {
  ((k - 273.15) * 1.8) + 32.0
}

fn main() {
  let args: Vec<String> = env::args().collect();

  // read in file name
  if args.len() != 2 {
    panic!("Error: Unexpected amount of arguments. Please provide a configuration file (TOML).");
  }
  let filename = &args[1];

  // read file contents into string
  let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

  let config: Config = match toml::from_str(&contents) {
    Ok(t) => t,
    Err(error) => panic!("Error: {:?}", error),
  };

  // Example query
  // api.openweathermap.org/data/2.5/weather?q=London,uk
  // https://api.openweathermap.org/data/2.5/weather? \
  // q=London,uk& \
  // appid=j6907e191s10s777f6r88a33333zz22
  let query = config.options.url + &config.options.city + "&appid=" + &config.options.key;
  let mut easy = Easy::new();
  match easy.url(&query) {
    Ok(t) => t,
    Err(error) => panic!("Error: {:?}", error),
  };

  let mut weather_json: String = String::new();
  {
    let mut transfer = easy.transfer();
    match transfer.write_function(|data| {
      weather_json = match String::from_utf8(Vec::from(data)) {
        Ok(json) => json,
        Err(error) => panic!("Error: Can't write data to JSON string: {:?}", error),
      };
      Ok(data.len())
    }) {
      Ok(t) => t,
      Err(error) => panic!("Error: Can't call transfer.write_function: {:?}", error),
    };

    match transfer.perform() {
      Ok(t) => t,
      Err(error) => panic!("Error: Can't call transfer.perform() {:?}", error),
    };
  };

  let w: WeatherData = match serde_json::from_str(&weather_json) {
    Ok(w) => w,
    Err(error) => panic!(
      "Error: There was a problem parsing the data with serde_json: {:?}",
      error
    ),
  };

  println!(
    "The temperature for {} is {}°F with {} skies and {}% humidity.",
    w.name,
    kelvin_to_fahrenheit(w.main.temp),
    if w.weather[0].main == "Clouds" {
      "cloudy".to_string()
    } else if w.weather[0].main == "Mist" {
      "misty".to_string()
    } else if w.weather[0].main == "Rain" {
      "rainy".to_string()
    } else {
      w.weather[0].main.to_string()
    },
    w.main.humidity
  );
}
